#def pig_latin(text):
#  say = []
  # Separate the text into words
#  words = text.split()
#  for word in words:
	  # Create the pig latin word and add it to the list
#	  say.append("{}{}ay".format(words[1:], words[0]))
      # Turn the list back into a phrase
#  return " ".join(say)
def pig_latin(text):
	words = text.split()
	say = []
	for word in words:
		say.append("{}{}ay".format(word[1:], word[0]))
	phrase = " ".join(say)
	return phrase
  
		
print(pig_latin("hello how are you")) # Should be "ellohay owhay reaay ouyay"
print(pig_latin("programming in python is fun")) # Should be "rogrammingpay niay ythonpay siay unfay"
