import os
import csv
import re

contains = []
replaced = []

with open ('emailids.csv', 'r') as f:
	for items in f.readlines():
		regex = r"[\w\.]+@abc.edu"
		result = re.search(regex, items)
		if result is not None:
			contains.append(items)

for emailids in contains:
       replaced.append(re.sub("([\w\.]+)@abc.edu", r"\1@xyz.edu", emailids))

for emailids in contains:
       print(emailids.strip())

print("\n")
print(replaced)
print("\n")

for emailids in replaced:
       print(emailids.strip())

print("\n")

with open('emailids.csv', 'a+') as f:
      csv_f = csv.writer(f)
      csv_f.writerows(replaced)
        
        
