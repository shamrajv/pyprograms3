#!/usr/bin/env python3

import re
def zip_code(pin):
    pattern = r"\d{5}-\d{4}"
    result = re.search(pattern,pin)
    if result is not None:
        return True
    else:
        return False

print(zip_code("39820-2057"))
print(zip_code("81r10-3142"))
print(zip_code("1234567890"))
print(zip_code("32154-87609"))
