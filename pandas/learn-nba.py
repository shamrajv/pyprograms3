import pandas as pd 

nba=pd.read_csv("nba.csv")


#Insert a column
#nba["Sport"] = "BasketBall"
nba.insert(loc = 0, column = "Sport", value = "BasketBall")
#print(nba.head())

#nba["Weight in KG"] = nba["Weight"].mul(0.4535)
#nba["Weight"] = nba["Weight"].mul(0.4535)

#Remove NaN rows from selected COlumn in a row, requires assigning back to dataframe
#nba=nba.dropna(subset=['College', 'Salary'])
print(nba)

#Fill some value in place of NaN in columbns using fillna function
nba["College"].fillna("Unknown", inplace = True)
print(nba)
