#!/bin/bash
n=0
command=$1
while ! $command && [ $n != 5 ]; do
	sleep 3
	((n+=1))
	echo "Retry #$n"
done
