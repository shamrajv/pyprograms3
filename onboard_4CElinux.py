#Author : Vivek Shamraj / HSC Linux
#!/usr/bin/env python
import os,subprocess,csv,json,shlex,sys,signal
from getpass import getpass
import requests
import time
import pdb

def unsetproxy():
   print("Unsetting http_proxy {0}".format(os.environ.pop('http_proxy', 'none')))
   print("Unsetting HTTP_PROXY {0}".format(os.environ.pop('HTTP_PROXY', 'none')))
   print("Unsetting https_proxy {0}".format(os.environ.pop('https_proxy', 'none')))
   print("Unsetting HTTPS_PROXY {0}".format(os.environ.pop('HTTPS_PROXY', 'none')))

def neworgs():
   neworglist = []
   with open('servers.csv', 'r') as rfile:
      datafile = csv.DictReader(rfile)
      for row in datafile:
         neworg = row['org_name']
         if neworg not in neworglist:
            neworglist.append(neworg)
   return(neworglist)

def reporting_linux(tpxid, patching_apitoken):
   with open('servers.csv','r') as rfile:
      data = csv.DictReader(rfile,delimiter=',')
      for row in data:
         orgname = row['org_name']
         url='https://{0}:{1}@jenkins-patching.global.tesco.org/jenkins/job/{2}/job/Reporting_Linux/build'.format(tpxid, patching_apitoken, orgname)
         try:
            ReportingJob = requests.post(url)
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            print("Reporting_Linux Job run for {0}".format(orgname))

def create_env(tpxid, osc_apitoken):
   envpayload = {}
   with open('servers.csv','r') as rfile:
      data = csv.DictReader(rfile,delimiter=',')
      for row in data:
         orgname = row['org_name']
         url = 'https://{0}:{1}@jenkins-osc.global.tesco.org/jenkins/job/{2}/job/Operations/job/create/job/create_environment/buildWithParameters'.format(tpxid, osc_apitoken, orgname)
         envpayload['ENVIRONMENT_NAME'] = row['env_name']
         envpayload['ENVIRONMENT_DESCRIPTION'] = row['env_description']
         try:
            envcreate = requests.post(url, envpayload)
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            print("Environment created in {0}".format(orgname))

def add_tags(tpxid, osc_apitoken):
   tagpayload = {}
   with open('servers.csv', 'r') as rfile:
      data = csv.DictReader(rfile, delimiter=",")
      for patchitem in data:
         orgname = patchitem['org_name']
         tagpayload = {'Nodes': patchitem['servername'], 'Tag':patchitem['tag'],'Environment':patchitem['env_name']}
         url='https://{0}:{1}@jenkins-osc.global.tesco.org/jenkins/job/{2}/job/Operations/job/create/job/set_tag_and_environment/buildWithParameters'.format(tpxid, osc_apitoken, orgname)
         try:
            tagaction = requests.post(url, tagpayload)
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            print("Tag Creation completed")

def refresh_org(tpxid, osc_apitoken):
   with open('servers.csv','r') as rfile:
      data = csv.DictReader(rfile,delimiter=',')
      for row in data:
         orgname = row['org_name']
         url = 'https://{0}:{1}@jenkins-osc.global.tesco.org/jenkins/job/{2}/job/Operations/job/create/job/00_get_org_config/build'.format(tpxid, osc_apitoken, orgname)
         try:
            refresh = requests.post(url)
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            print("Organization Refresh completed")

def add_tpx_id(tpxid, osc_apitoken):
   #pdb.set_trace()
   userpayload = {}
   with open('servers.csv','r') as rfile:
      data = csv.DictReader(rfile,delimiter=',')
      for row in data:
         userpayload['CHEF_ORG_NAME'] = row['org_name']
         userpayload['USERNAME'] = row['svc_name']
         userpayload['ACTION'] = "add"
         userpayload['USER_ROLE'] = "admin"
         orgname = row['org_name']
         useradm_url = "https://{0}:{1}@jenkins-osc.global.tesco.org/jenkins/job/{2}/job/Admin/job/user_management/buildWithParameters".format(tpxid, osc_apitoken, orgname)
         try:
            useradm = requests.post(useradm_url, userpayload)
            useradm.raise_for_status()
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            print("User {0} added successfully to {1} org".format(userpayload['USERNAME'], userpayload['CHEF_ORG_NAME']))

def create_org(tpxid, apitoken, orgjob_url):
   org_createdlist = []
   valuelist = []
   url = orgjob_url.format(tpxid, apitoken)
   orgload = {"parameter": [{"name":"OrgJSON", "value":'{"orgname":"orgname"}'} ]}
   with open('servers.csv','r') as rfile:
      data = csv.DictReader(rfile,delimiter=',')
      for row in data:
         orgname = "\"%s\"" % row['org_name']
         value = "{"+orgname+":"+orgname+"}"
         if value not in valuelist:
            valuelist.append(value)
      for orgvalues in valuelist:
         orgload['parameter'][0]['value'] = orgvalues
         orgpayload = {'json': json.dumps(orgload)}
         try:
            org_creation = requests.post(url, orgpayload)
            org_creation.raise_for_status()
         except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
         except Exception as err:
            print("Exception occurred: " + str(err))
         else:
            #print("Org creation successful: {0}".format(orgname))
            org_createdlist.append(orgvalues)
      return(org_createdlist)

def append_server_details(datadict):
    with open('server_details.json', 'w') as wfile:
       json.dump(datadict, wfile, sort_keys=True, indent=4, separators=(',', ': '))

def create_server_details(filename):
   datadict = {}
   with open(filename, 'r') as data_file:
      data = csv.DictReader(data_file, delimiter=",")
      for row in data:
         item = datadict.get(row["org_name"], dict())
         item[row["servername"]] = row["svc_name"]
         datadict[row["org_name"]] = item
         append_server_details(datadict)

def exec_onboard_script():
   try:
      subprocess.check_call(shlex.split("sudo /opt/chefdk/embedded/bin/ruby /appl/chef_repo/chef_osc_other_orgs/bootstrap_other_orgs.rb"))
   except subprocess.CalledProcessError as err:
      print("Onboarding Error: " + str(err))

def main():
   osc_orgjob_url = "https://{0}:{1}@jenkins-osc.global.tesco.org/jenkins/job/ChefHA_Admin_Activities_CE_Linux/job/bulk_org_create/build"
   patching_orgjob_url = "https://{0}:{1}@jenkins-patching.global.tesco.org/jenkins/job/ChefHA_Admin_Activities_CE_Linux/job/bulk_folder_creation_projectwise/build"
   if len(sys.argv) > 1:
      action = sys.argv[1]
   else:
      print("Usage: python onboard2_chefpatching.py <createorg | onboard2org>")
      sys.exit(1)
   tpxid = raw_input("Enter the TPX id : ")
   print("You entered : " + tpxid)
   osc_apitoken = getpass(prompt = "Enter the OSC portal API token for above TPX id : ")
   patching_apitoken = getpass(prompt = "Enter the Patching portal API token for above TPX id : ")
   print("\n\n")
   unsetproxy()
   print("\n\n")

   if action == "createorg":
      #On jenkins-osc portal
      print("Executing create Organization(s) API")
      newuniqorgs = neworgs()
      for orgs in newuniqorgs:
         print("Organization {0} will be created in Jenkins-OSC portal" .format(orgs))
         print
      org_status = create_org(tpxid, osc_apitoken, osc_orgjob_url)
      for org in org_status:
         print("Created Organization(s) {0}".format(org))
      print("\n\n")
      print("Creating Org Structure on Jenkins-Patching portal")
      #On jenkins-patching portal
      org_status = create_org(tpxid, patching_apitoken, patching_orgjob_url)
      for org in org_status:
         print("Created Organization(s) {0} in jenkins-patching portal".format(org))
      sys.exit(0)

   elif action == "onboard2org":
      print("Creating User Accounts, Onboarding Servers, Environments and Tags using servers.csv file")
      response = raw_input("Please make sure the Organization were created already, Enter Yes or No :")
      if response == "Yes":
         print("Adding TPX id")
         add_tpx_id(tpxid, osc_apitoken)
         #Onboard Servers in OSC portal
         print("Bootstrapping servers to OSC Portal")
         create_server_details("servers.csv")
         exec_onboard_script()
         #Refresh the organizations
         refresh_org(tpxid, osc_apitoken)
         print("Refresh Job Issued to Organizations, sleeping for 2 minutes")
         time.sleep(120)
         #On jenkins OSC portal
         print("Creating Environment and Tags")
         create_env(tpxid, osc_apitoken)
         print("Refreshing Org")
         refresh_org(tpxid, osc_apitoken)
         time.sleep(15)
         add_tags(tpxid, osc_apitoken)
         refresh_org(tpxid, osc_apitoken)
         print("Refresh Job Issued to Organizations")
         #On jenkins-patching portal
         print("\n\n")
         print("Running Reporting_Linux job in patching portal for all orgs")
         reporting_linux(tpxid, patching_apitoken)
      elif response == "No":
         sys.exit(1)
      else:
         print("Please Enter 'Yes' or 'No'")
         sys.exit(1)
   else:
      print("Usage: python onboard2_chefpatching.py <createorg | onboard2org>")

try:
   main()
except KeyboardInterrupt:
   print "User Interrupt Recieved, Exiting"
   sys.exit(1)
