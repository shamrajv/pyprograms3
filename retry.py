def retry(operation, attempts):
  for n in range(attempts):
    if operation():
      print("Attempt " + str(n) + " succeeded")
      return
    else:
      print("Attempt " + str(n) + " failed")

def create_user():
	return True
	
def stop_service():
	return True
	
retry(create_user, 3)
retry(stop_service, 5)

"""The retry function tries to execute an operation that might fail, 
it retries the operation for a number of attempts. Currently the code will 
keep executing the function even if it succeeds. Fill in the blank so the 
code stops trying after the operation succeeded"""
