#!/usr/bin/env python3
from PIL import Image
import os, glob, shutil

size = (128, 128)
tiffs = glob.glob('/home/student-04-03cac3b0c1bf/images/*')

for file in tiffs:
    outfile = os.path.splitext(file)[0] + ".jpg"
    if file != outfile:
        try:
            im = Image.open(file)
            im.thumbnail(size)
            im.rotate(270, expand='True')
            imc = im.convert('RGB')
            imc.save(outfile, "JPEG")
        except OSError as erro:
            print("Could not convert {} to Jpeg form: {}".format(file, str(erro)))

files = glob.glob('/home/student-04-03cac3b0c1bf/images/*')
for jpgs in files:
    if jpgs.endswith('jpg'):
        shutil.move(jpgs, '/opt/icons')
