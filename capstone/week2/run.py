#!/usr/bin/env python3

import os, requests

files = os.listdir('/data/feedback')
postdata = {}

for feedback in files:
    feedback = "/data/feedback/" + feedback
    with open(feedback, 'r') as filer:
        postdata['title'] = filer.readline().strip()
        postdata['name'] = filer.readline().strip()
        postdata['date'] = filer.readline().strip()
        postdata['feedback'] = filer.readline().strip()
    #print(postdata)
        try:
            webreply = requests.post('http://34.67.15.127/feedback/', data=postdata)
            if webreply.status_code == 201:
                print("Post Request Succeeded for {}".format(feedback))
            else:
                print("Feedback Post Request Failed for {}".format(feedback))
            print("Post request status : {}".format(webreply.status_code))
            print("Web reply Text:-")
            print(webreply.text)
            print()
        except:
            webreply.raise_for_status()
