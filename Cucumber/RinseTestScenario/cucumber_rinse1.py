import os, csv, sys, signal
import pandas as pd

def rewrite_dflocation(verifiedlist, location, test_scenarios):
    for testscenarios in verifiedlist:
        test_scenarios.at[location, 'Cucumber Scenario'] = '\n'.join(verifiedlist)
    return test_scenarios

def rinse_scenarios(test_scenarios, rows):
    i = 0
    for i in range(rows):
        list = test_scenarios.loc[i]["Cucumber Scenario"].split("\n")
        newlist=[]
        for index,value in enumerate(list):
            if list[index].startswith("Given") or list[index].startswith("Then") or list[index].startswith("And") or list[index].startswith("When"):
                newlist.append(list[index])
            else:
                prev_index = index - 1
                value1 = list[prev_index]
                value2 = list[index]
                newvalue = value1 + " " + value2
                del_item = newlist.pop(prev_index)
                newlist.insert(prev_index, newvalue)
                processed_scenarios = rewrite_dflocation(newlist, i, test_scenarios)
        return processed_scenarios


def main():
    try:
        inputfile = sys.argv[1] 
    except IndexError as err:
        print("Usage: scriptname input_filename")
        sys.exit(1)
    os.chdir("C:\\Users\\SESA731553\\Documents\\python\\Cucumber\\RinseTestScenario")
    test_scenarios = pd.read_csv(inputfile)
    rows, columns = test_scenarios.shape
    test_scenarios["Cucumber Scenario"] = test_scenarios["Cucumber Scenario"].astype('str')
    rinsed_scenarios = rinse_scenarios(test_scenarios, rows)
    rinsed_scenarios.to_csv("C:\\Users\\SESA731553\\Documents\\python\\Cucumber\\RinseTestScenario\\Rinsed_Scenarios.csv")


try:
   main()
except KeyboardInterrupt:
   print("User Interrupt Recieved, Exiting")
   sys.exit(1)
