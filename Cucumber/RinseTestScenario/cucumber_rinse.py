import os, csv, sys, json
import pandas as pd

def pop_ins(newlist, prev_index):
		del_item = newlist.pop(prev_index)
    	newlist.insert(prev_index, newvalue)
    return newlist

def rmempty_string(list):
	for i in list:
		if i in "":
			list.remove(i)
	return list
	#[list for i in list if i in "" list.remove(i)]


def rewrite_dflocation(verifiedlist, location, df):
    for testscenarios in verifiedlist:
        df.at[location, 'Cucumber Scenario'] = '\n'.join(verifiedlist)
    return df

#Change this location to your directory having Cucumber Scenario excel sheet
os.chdir("C:\\Users\\SESA731553\\Documents\\python\\Cucumber\\RinseTestScenario")
#Change the input file name
df = pd.read_csv("issues.csv")
rows, columns = df.shape
df["Cucumber Scenario"] = df["Cucumber Scenario"].astype('str')

i = 0
for i in range(rows):
    list = df.loc[i]["Cucumber Scenario"].split("\n")
    import pdb ; pdb.set_trace()
    print(list)
    list = rmempty_string(list)
    print(list)
    newlist=[]
    for index,value in enumerate(list):
        if list[index].startswith("Given") or list[index].startswith("Then") or list[index].startswith("And") or list[index].startswith("When"):
            newlist.append(list[index])
        else:
            prev_index = index - 1
            print("Previous Index {}".format(prev_index))
            value1 = list[prev_index]
            value2 = list[index]
            newvalue = value1 + " " + value2
            print("Newvalue {}".format(newvalue))
            if newlist[prev_index]:
            	newlist = pop_ins(newlist, prev_index)
            else:
            	prev_index = prev_index - 1
            	newlist = pop_ins(newlist, prev_index)

    processed_df = rewrite_dflocation(newlist, i, df)

#print(processed_df)
processed_df.to_csv("C:\\Users\\SESA731553\\Documents\\python\\Cucumber\\RinseTestScenario\\newsheet.csv")
