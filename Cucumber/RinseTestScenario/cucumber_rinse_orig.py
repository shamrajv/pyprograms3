import os, csv, sys, json
import pandas as pd

def rewrite_dflocation(verifiedlist, location, df):
    for testscenarios in verifiedlist:
        df.at[location, 'Cucumber Scenario'] = '\n'.join(verifiedlist)
    return df

#Change this location to your directory having Cucumber Scenario excel sheet
os.chdir("C:/Clive/Cucumber/Jira/Cucumber")

df = pd.read_csv("issues.csv")
rows, columns = df.shape
df["Cucumber Scenario"] = df["Cucumber Scenario"].astype('str')

i = 0
for i in range(rows):
    list = df.loc[i]["Cucumber Scenario"].split("\n")
    newlist=[]
    for index,value in enumerate(list):
        if list[index].startswith("Given") or list[index].startswith("Then") or list[index].startswith("And") or list[index].startswith("When"):
            newlist.append(list[index])
            print(index)
        else:
            prev_index = index - 1
            value1 = list[prev_index]
            value2 = list[index]
            newvalue = value1 + " " + value2
            del_item = newlist.pop(prev_index)
            newlist.insert(prev_index, newvalue)
    processed_df = rewrite_dflocation(newlist, i, df)

#print(processed_df)
processed_df.to_csv("C:/Clive/Cucumber/Jira/Cucumber/Cucumber.csv")
