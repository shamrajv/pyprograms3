def rewrite_dflocation(verifiedlist, location, df):
    #df = pd.read_csv("C:\\Users\\SESA731553\\Documents\\python\\Cucumber\\Cucumber-updated-preprod-1.csv")
    #df.loc[location]
    for testscenarios in verifiedlist:
        #print(testscenarios)
        #df.loc[location, 'Cucumber Scenario'] = testscenarios
        #print(df.loc[location]["Cucumber Scenario"])
        df.at[location, 'Cucumber Scenario'] = '; '.join(verifiedlist)
    return df


for i in range(rows):
    list = df.loc[i]["Cucumber Scenario"].split("\n")
    newlist=[]
    for index,value in enumerate(list):
        if list[index].startswith("Given") or list[index].startswith("Then") or list[index].startswith("And") or list[index].startswith("When"):
            newlist.append(list[index])
            #newlist.append(list[index])
        else:
            prev_index = index - 1
            value1 = list[prev_index]
            value2 = list[index]
            newvalue = value1 + " " + value2
            del_item = newlist.pop(prev_index)
            newlist.insert(prev_index, newvalue)
    #print(newlist)
    processed_df = rewrite_dflocation(newlist, i, df)


print(processed_df)
df.to_csv("C:\\....\\newsheet.csv")
    
['Given I have a 16A puck ', 'When I create a new schedule', 'Then I can select my 16A puck from the  list of appliances', 'And create a schedule to turn the 16A puck off']
['Given I have a 16A puck ', 'When I create a new schedule', 'Then I can select my 16A puck from the list of appliances', 'And create a schedule to turn the 16A puck on']
['Given I have a Digistat Connect device', 'And the Holiday Mode is currently Enabled', 'And Today is within the current Holiday Period', 'And I have accessed the Holiday mode option (HOL + Person Outside of House is blinking) from under User Settings', 'When I keep the Holiday Mode = Enabled', 'And I enter a new set of Holiday Start Date / Time values GT Today / Now', 'And I enter a new set of Holiday End Date/Time values > Holiday Start date/Time', 'And I enter a valid Target Temperature value', 'Then on returning to the Home Screen I should NOT see an Away / Holiday icon (House with a person outside) as the Holiday Period is not active']
['Given I have a Digistat Connect device', 'And the Holiday mode is currently disabled', 'And I make a note of the current Ambient Temperature', 'And I have accessed the Holiday mode option (HOL + Person Outside of House is blinking) from under User Settings', 'When I enter a new set of Holiday Start Date / Time values = Today / Now', 'And I entered a new set of Holiday End Date / Time values > Holiday Start Date / Time', 'And I set the Holiday Temperature Setpoint to > the current Ambient Temperature', "And I confirm the setting using 'O'", 'Then on returning to the Home screen I should see the Holiday icon (House with the person outside)', 'And I should see the CH icon (flame) activated']