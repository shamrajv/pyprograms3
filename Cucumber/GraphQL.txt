{

    getTestSets(jql: "project = 'WISER'", limit: 100, start:101) {

        total

        results {

            jira(fields:["key"])

            tests(limit: 100) {

                results {

                    jira(fields: ["key"])

                }

            }

        }

    }

}

====================================================
{
    getPrecondition(issueId: "79300") {
        jira(fields: ["key"])

        tests(limit:10){
        results {
        jira(fields:["key"])
					
				}
    }
}
}
====================================================

