#Script By	: Yash Mardikar 
#Purpose	: Change usernames of Jira user using rest api from CSV file
#How to use	: Update CSV path, Jira hostname, Username and Password in script
#			  CSV file must have headers "username" and "new_username" 
#			  Written for Python 3+ 


import csv, json, requests
import time
from pprint import pprint
from pathlib import Path

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

############# UPDATE THIS ######################
mapping_csv_path=r"C:\Users\SESA731553\Downloads\fullname_update.csv"

#jr_hostname     =	"http://URL/rest/api/latest"                     #New Migration Server
jr_hostname     =	"http://10.236.34.41:8080/rest/api/latest"       #Preprod
jr_uname        =   "kalyan"
jr_pwd          =   "kalyan"
verify			=	False		#False for PreProd
################################################

auth        =   (jr_uname,jr_pwd)
headers     =   {"Accept" : "application/json","Content-Type" : "application/json",}

def read_csv_data(fname=None):
    '''takes csv file as input return list of dict rows as output''' 
    #check if fname exists 
    if fname is None:
        raise Exception("fname argument cannot be None")
    #?check if fname is valid filename
    list_of_rows=[]
    with open(fname,encoding="utf8") as f:
        reader_csv= csv.DictReader(f)    
        for row in reader_csv:
            list_of_rows.append(row)
    return list_of_rows
	
USER_ALREDY_EXISTS_ERROR= '{"errorMessages":[],"errors":{"username":"A user with that username already exists."}}'

user_mapping_csv = read_csv_data(mapping_csv_path)
failed_request_list=[]
user_already_exists=[]

for data in user_mapping_csv:
	usernm_old	=	data['username']
	print(usernm_old)
	new_fullname	=	data['new_fullname']
	print(new_fullname)
	user_url	=	f"{jr_hostname}/user?username={usernm_old}"
	#payload	=	{"displayName" : new_fullname}
	payload	=	{"displayName" : data['new_fullname']}
	try:
		r = requests.put(user_url,auth = auth,data=json.dumps(payload),headers = headers,verify=verify)
		if r:		#successful response
			print(f"Success {new_fullname}:{usernm_old}")
		elif r.status_code == 400 and r.text == USER_ALREDY_EXISTS_ERROR:
			print(f"Already Exists {new_fullname}:{usernm_old}")
			user_already_exists.append((new_fullname,usernm_old))
		else:
			print("ERROR :" + str(r.raise_for_status()))
			#print(f"Failure {new_fullname}:{usernm_old}")
			#print(r,r.tex`t[0:100])
			#print(r.json)
			failed_request_list.append((new_fullname,usernm_old))
	except requests.exceptions.HTTPError as httperr:
		print("HTTP error occurred: " + str(httperr))
		print("Connection Error")
		print(f"Failure {new_fullname}:{usernm_old}")
		failed_request_list.append((new_fullname,usernm_old))
		time.sleep(0.4)

print("List of Failed username Updates:")
print(failed_request_list)
print("List of Failed username Updates due to preexisting tartget username:")
print(user_already_exists)

	
		

