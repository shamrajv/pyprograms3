#!/usr/bin/env python3

import re
import csv
import os
import sys
import operator


error = {}
with open('syslog.log', 'r') as logfile:
    for line in logfile:
        result = re.search(r"ticky: ERROR ([\w ]*)", line)
        if result is not None:
            if result[0] not in error:
                error[result[0]] = 1
            else:
                error[result[0]] += 1

#print(sorted(errors.items(), key=operator.itemgetter(1),reverse=True))
#print('\n')
with open('error_message', 'w+') as errfile:
    heading = "Error" + "," + "Count"
    errfile.write(heading + '\n')
    for items,val in sorted(error.items(), key=operator.itemgetter(1),reverse=True):
        #print("{},{}".format(items, val))
        values = items + "," + str(val)
        errfile.write(values + '\n')
