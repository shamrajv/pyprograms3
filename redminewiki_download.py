import os
import sys
import re
import docx
import os.path
import glob, pdb
from PIL import Image
from bs4 import BeautifulSoup
from urllib.request import urlretrieve
from urllib.error import HTTPError
import requests


def txt2docx(inputfile, outputfile, workpath):
    # os.chdir(workpath)
    document = docx.Document()
    with open(inputfile, 'r') as ifile:
        textline = ifile.read()
    document.add_paragraph(textline)
    document.save(outputfile)


def downloadatt(url, workpath):
    # os.chdir(workpath)
    res = requests.get(url)
    soup = BeautifulSoup(res.text, "html.parser")
    listofhrefs = []
    listofpngs = []
    for link in soup.find_all('a'):
        listofhrefs.append(link.get('href'))
    for href in listofhrefs:
        if re.match("(/redmine/attachments/download/\w+)", str(href)):
            listofpngs.append(href)
    for unique_pngs in set(listofpngs):
        pngfile = unique_pngs.split("/")[-1]
        basepngurl = f"http://redmine.com:8080/{unique_pngs}"
        try:
            urlretrieve(basepngurl, pngfile)
        except urllib.error.HTTPError as e:
            print(f'HTTP Error while downloading attachments: {e.code}')
        except urllib.error.URLError as e:
            print(f'URL Error while downloading attachments: {e.reason}')


def compressImage(images, max_width, max_height, imgpath, oimgpath):
    for pngs in images:
        #print("Image is {}".format(pngs))
        pngs = os.path.basename(pngs)
        #pdb.set_trace()
        img = Image.open(os.path.join(imgpath, pngs))
        width, height = img.size
        aspect_ratio = width / height
        if width > max_width:
            width = max_width
            height = int(max_width/aspect_ratio)
        if height > max_height:
            height = max_height
            width = int(height * aspect_ratio)
        resized_img = img.resize((width, height))
        oimgpath = os.path.abspath(oimgpath)
        resized_img.save(os.path.join(oimgpath, pngs), format='PNG')


def outputdocx(outputfile, convertedfile, workpath, oimgpath):
    # os.chdir(workpath)
    olddoc = docx.Document(outputfile)
    doclines = olddoc.paragraphs[0].text.split("\n")
    newdoc = docx.Document()
    for items in doclines:
        if not re.search("png", items):
            newdoc.add_paragraph(items)
        else:
            items = items.split("!", 2)[1]
            oimages = f'{oimgpath}/{items}'
            newdoc.add_picture(os.path.realpath(oimages))
    newdoc.save(convertedfile)


def main():
    if len(sys.argv) > 2:
        # print("Arg1 is {}".format(sys.argv[1]))
        projectname = sys.argv[1]
        wikitxtname = sys.argv[2]
        print("Project Name is {}".format(sys.argv[1]))
        print("Downloaded Wiki.txt will named as {}".format(wikitxtname))
    else:
        print(
            "Usage : python WikiProcess.py <projectString, ex jedi> <wiki.txt alias name>")
        sys.exit(1)
    max_width = 900
    max_height = 1200
    # inputfile = f"{projectname}.txt"
    outputfile = f"{projectname}.docx"
    convertedfile = f"converted_{outputfile}"
    workpath = os.path.join(os.curdir, projectname)
    workpath = os.path.abspath(workpath)
    if not os.path.isdir(workpath):
        os.mkdir(workpath)
    imgpath = os.path.join(os.curdir, projectname)
    imgpath = os.path.abspath(imgpath)
    oimgpath = f'{workpath}/images'
    oimgpath = os.path.abspath(oimgpath)
    if not os.path.isdir(oimgpath):
        os.mkdir(oimgpath)
    url = f"http://redmine.com:8080/redmine/projects/{projectname}/wiki"
    wikitxturl = f"http://redmine.com:8080/redmine/projects/{projectname}/wiki/{wikitxtname}"
    os.chdir(workpath)
    try:
        urlretrieve(wikitxturl, wikitxtname)
    except urllib.error.HTTPError as e:
        print(f'HTTP Error while downloading wiki txt file: {e.code}')
    except urllib.error.URLError as e:
        print(f'URL Error while downloading wiki txt file: {e.reason}')
    txt2docx(wikitxtname, outputfile, workpath)
    downloadatt(url, workpath)
    images = glob.glob(f'{workpath}/*.png')
    print(images)
    #pdb.set_trace()
    compressImage(images, max_width, max_height, imgpath, oimgpath)
    outputdocx(outputfile, convertedfile, workpath, oimgpath)


try:
    main()
except KeyboardInterrupt:
    print("User Interrupt Recieved, Exiting")
    sys.exit(1)
