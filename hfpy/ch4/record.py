#!/usr/bin/env python3
import os
import sys
import pickle

man =[]
otherman = []

try:
    with open('sketch.txt', 'r') as file:
        for line in file:
            if line.find(':') is not -1:
                (role, line_spoken) = line.split(':', 1)
                line_spoken = line_spoken.strip() #this is example of string being immutable in python
                #print ("{} spoke {}".format(role,line_spoken))
                if role == "Man":
                    man.append(line_spoken)
                elif role == "Other Man":
                    otherman.append(line_spoken)
except IOError as erro:
    print("Input File Error: " + str(erro))
#print(man)
#print()
#print(otherman)
try:
    with open("man_file", 'wb') as manfile, open("otherman_file", 'wb') as omanfile: #Open multiple files, using `with and comma`
        pickle.dump(man, file=manfile)
        pickle.dump(otherman, file=omanfile)

except IOError as erro:
    print("Output File Error: " + str(erro))

except pickle.PickleError as perr:
    print('Pickling error:' + str(perr))
