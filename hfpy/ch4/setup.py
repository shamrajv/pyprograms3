from distutils.core import setup

setup (
        name    =   'nester',
        version =   '1.0.0',
        py_modules =   ['nester'],
        author  =   'hfpy',
        author_email    =   'viveksham@gmail.com',
        description =   'A simple printer of nested list',
        )

