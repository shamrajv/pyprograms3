#!/usr/bin/env python3.6

def get_coachdata(filename):
    try:
        with open(filename) as f:
            data = f.readline()
        return (data.strip().split(',')) # Here, a list is returned
    except IOError as erro:
        print("IOError: " + str(erro))
        return(None)

def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return(time_string)
    (mins,sec) = time_string.split(splitter)
    return(mins + "." + sec)


sarah = get_coachdata('sarah2.txt') #sarah is a list
sarah_data = {}
sarah_data['Name'] = sarah.pop(0)
sarah_data['DOB'] = sarah.pop(0)
sarah_data['Times'] = sarah
print(sarah_data['Name'] + "'s fatest times are :'" + str(sorted(set([sanitize(t) for t in sarah_data['Times']]))[:3]))
