#!/usr/bin/env python3.6

class AthleteList(list):
    def __init__(self, a_name, a_dob='None', a_times=[]):
        list.__init__([])
        self.name = a_name
        self.dob = a_dob
        self.extend(a_times) # The object instance is an AthleteList Variable
                             # hence it is an append operation on self variable
                             # name

    def top3(self):
        return(sorted(set([sanitize(t) for t in self]))[:3])

def get_coachdata(filename):
    try:
        with open(filename) as f:
            data = f.readline()
            templ = (data.strip().split(','))
            return(AthleteList(templ.pop(0), templ.pop(0), templ))
    except IOError as erro:
        print("IOError: " + str(erro))
        return(None)

def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return(time_string)
    (mins,sec) = time_string.split(splitter)
    return(mins + "." + sec)

sarah = get_coachdata('sarah2.txt') #sarah is a AthleteList class object
print(sarah.name + "'s fatest times are :" + str(sarah.top3()))
julie  = get_coachdata('julie2.txt') #julie is a AthleteList class object
print(julie.name + "'s fatest times are :" + str(julie.top3()))
mikey  = get_coachdata('mikey2.txt') #mikey is a AthleteList class object
print(mikey.name + "'s fatest times are :" + str(mikey.top3()))
james  = get_coachdata('james2.txt') #james is a AthleteList class object
print(james.name + "'s fatest times are :" + str(james.top3()))

vikku = AthleteList('Vivek Shamraj')
vikku.append('1.31')
print(vikku.top3())
vikku.extend(['2.22', '1-21', '2:22'])
print(vikku.name + "'s fastest times are :" + str(vikku.top3()))
