#!/usr/bin/env python3.6

def get_coachdata(filename):
    try:
        with open(filename) as f:
            data = f.readline()
        templ = (data.strip().split(',')) # Here, a list is returned
        return({'Name': templ.pop(0),
                'DOB': templ.pop(0),
                'Times' : str(sorted(set([sanitize(t) for t in templ]))[:3])})
    except IOError as erro:
        print("IOError: " + str(erro))
        return(None)

def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return(time_string)
    (mins,sec) = time_string.split(splitter)
    return(mins + "." + sec)


sarah = get_coachdata('sarah2.txt') #sarah is a dict
print(sarah['Name'] + "'s fatest times are :'" + sarah['Times'])
julie  = get_coachdata('julie2.txt') #julie is a dict
print(julie['Name'] + "'s fatest times are :'" + julie['Times'])
mikey  = get_coachdata('mikey2.txt') #mikey is a dict
print(mikey['Name'] + "'s fatest times are :'" + mikey['Times'])
james  = get_coachdata('james2.txt') #james is a dict
print(james['Name'] + "'s fatest times are :'" + james['Times'])
