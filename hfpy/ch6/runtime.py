#!/usr/bin/env python3.6

def get_coachdata(filename):
    try:
        with open(filename) as f:
            data = f.readline()
        return (data.strip().split(','))
    except IOError as erro:
        print("IOError: " + str(erro))
        #return(None)

def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return(time_string)
    (mins,sec) = time_string.split(splitter)
    return(mins + "." + sec)


sarah = get_coachdata('sarah2.txt')
(name, sarah_dob) = sarah.pop(0), sarah.pop(0)
print(name + "'s fatest three times are: " + str(sorted(set([sanitize(t)for t in sarah]))[:3]))
