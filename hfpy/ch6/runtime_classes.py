#!/usr/bin/env python3.6
class Athlete:
    def __init__(self, a_name, a_dob=None, a_times=[]):
        self.name = a_name
        self.dob = a_dob
        self.times = a_times
#Class method's first argument is always self
    def top3(self):
        return(sorted(set([sanitize(t) for t in self.times]))[:3])

    def add_time(self, t):
        self.times.append(t)

    def add_times(self, list_of_times):
        self.times.extend(list_of_times)

def get_coachdata(filename):
    try:
        with open(filename) as f:
            data = f.readline()
            templ = (data.strip().split(',')) # Here, a list is returned
            return(Athlete(templ.pop(0), templ.pop(0), templ))
                # str(sorted(set([sanitize(t) for t in templ]))[:3])})
    except IOError as erro:
        print("IOError: " + str(erro))
        return(None)

def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return(time_string)
    (mins,sec) = time_string.split(splitter)
    return(mins + "." + sec)


sarah = get_coachdata('sarah2.txt') #sarah is a Athlete class object
print(sarah.name + "'s fatest times are :" + str(sarah.top3()))
julie  = get_coachdata('julie2.txt') #julie is a Athlete class object
print(julie.name + "'s fatest times are :" + str(julie.top3()))
mikey  = get_coachdata('mikey2.txt') #mikey is a Athlete class object
print(mikey.name + "'s fatest times are :" + str(mikey.top3()))
james  = get_coachdata('james2.txt') #james is a Athlete class object
print(james.name + "'s fatest times are :" + str(james.top3()))
