#!/usr/bin/env python3.6
import re
import os

def sanitize(time_string):
    if time_string.find(':') is not -1:
            splitter = ':'
    elif time_string.find('-') is not -1:
            splitter = '-'
    else:
        return time_string
    (minutes, seconds) = time_string.split(splitter)
    return (minutes + "." + seconds)


def create_list(the_file):
    the_list = re.sub(r"(\w+)\.txt", r"\1", the_file)
    with open(the_file, 'r') as fobj:
        for line in fobj:
            the_list = line.strip().split(',')
    return [sanitize(each_items) for each_items in the_list]

print(sorted(set(create_list('sarah.txt')))[:3])
print(sorted(set(create_list('julie.txt')))[:3])
print(sorted(set(create_list('mikey.txt')))[:3])
print(sorted(set(create_list('james.txt')))[:3])
