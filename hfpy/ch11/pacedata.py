#!/usr/bin/env python3.6
from find_it import find_closest
from tm2secs2tm import secs2time, time2secs, format_time

def find_nearest_time(look_for, target_data):
    what = time2secs(look_for)
    where = [ time2secs(t) for t in target_data]
    res = find_closest(what, where)
    return(secs2time(res))

row_data = {}

with open('PaceData.csv', 'r') as f:
   column_heading = f.readline().strip().split(',')
   column_heading.pop(0)
   for each_line in f:
      row = each_line.strip().split(',')
      row_label = row.pop(0)
      inner_dict = dict()
      for i in range(len(column_heading)):
          inner_dict[format_time(row[i])] = column_heading[i]
      row_data[row_label] = inner_dict

#predicton = [k for k in row_data['20k'].keys() if row_data['20k'][k] == column_heading]
#print(prediction)
#try:
distance_run = input("Enter the Distance Attempted: ")
recorded_time = input("Input the recorded time: ")
predicted_distance = input("Enter the distance you want a prediction for: ")
    #print(row_data[distance_run][recorded_time])

#except KeyError as erro:
#    print("Absent Key: " + str(erro))

closest_time = find_nearest_time(format_time(recorded_time), row_data[distance_run])
closest_column_heading = row_data[distance_run][closest_time]

prediction = [k for k in row_data[predicted_distance].keys() if row_data[predicted_distance][k] == closest_column_heading]

print('The predicted time running ' + predicted_distance + ' is: ' + prediction[0] + ' .')
