#!/usr/bin/env python3.6
def combine_guests(guests1, guests2):
	cguests = {}
	for key, value in guests1.items():
		if key not in guests2 and key not in cguests:
			cguests[key] = value
		if key in guests2 and key not in cguests:
			cguests[key] = value
			guests2.pop(key)
	for items, val in guests2.items():
		if items not in cguests:
			cguests[items] = val

	return (cguests)

  # Combine both dictionaries into one, with each key listed
  # only once, and the value from guests1 taking precedence

Rorys_guests = { "Adam":2, "Brenda":3, "David":1, "Jose":3, "Charlotte":2, "Terry":1, "Robert":4}
Taylors_guests = { "David":4, "Nancy":1, "Robert":2, "Adam":1, "Samantha":3, "Chris":5}

print(combine_guests(Rorys_guests, Taylors_guests))
