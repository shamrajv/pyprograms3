#!/usr/bin/env python3.6

def validate_user(username, minlen):
    """Checks Username input for minimum of 3 strings in len
       and validates that it is a string variable. Uses assert and raise"""
    assert type(username) == str, "Username needs to be string"
    if minlen < 1:
        raise ValueError("minlen of name should be at least 3")
    if len(username) < minlen:
        return False
    if not username.isalnum():
        return False
    return True
