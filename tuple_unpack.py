def guest_list(guests):
	for items in guests:
		(name, age, prof) = items
		print("{} is {} years old and works as {}".format(name,age,prof))

guest_list([('Ken', 30, "Chef"), ("Pat", 35, 'Lawyer'), ('Amanda', 25, "Engineer")])
