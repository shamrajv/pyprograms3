#!/usr/bin/env python3.6
class Elevator:
    def __init__(self, bottom, top, current):
        """Initializes the Elevator instance."""
        self.bottom = bottom
        self.top = top
        self.current = current
    def __str__(self):
		return "Current Floor: {}".format(self.current)
    def up(self):
		if self.current != 10:
			"""Makes the elevator go up one floor."""
			self.current += 1
    def down(self):
		if self.current != -1:
			"""Makes the elevator go down one floor."""
			self.current -= 1
    def go_to(self, floor):
        """Makes the elevator go to the specific floor."""
        self.current = floor

elevator = Elevator(-1, 10, 0)

#elevator.up() 
#print(elevator.current) #should output 1

#elevator.down() 
#print(elevator.current) #should output 0

#elevator.go_to(10) 
#print(elevator.current) #should output 10

elevator.go_to(10)
elevator.up()
elevator.down()
print(elevator.current) # should be 9
# Go to the bottom floor. Try to go down, it should stay. Then go up.
elevator.go_to(-1)
elevator.down()
elevator.down()
elevator.up()
elevator.up()
print(elevator.current) # should be 1

elevator.go_to(5)
print(elevator)
