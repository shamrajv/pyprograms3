#!/usr/bin/env python3.6
class Mangoes:
	name = "Banganapalli" 
	def __init__(self, size, flavor, name):
		self.size = size
		self.flavor = flavor
		self.name = name
	def __str__(self):
		return ("This Mangoe, {}, grows very {} and is {}".format(self.name, self.size, self.flavor))
		
Alphonso = Mangoes("small", "very sweet", "Alphonso")
print (Alphonso)
Chausa = Mangoes("Lean", "very juicy", "Chausa")
print (Chausa)
Banganapalli = Mangoes("Big", "very juicy")
