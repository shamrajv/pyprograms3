import pandas as pd
import csv

def texmex(df, dict1):
    line = []
    issue_id = {}
    for key, value in dict1.items():
        listofcomment_index = value.to_list()
        for each_index in listofcomment_index:
            line.append(df.loc[each_index]["Comments"])
            #row = str(key)+", " + "  ".join(line)
            #print(row, end = "\n")
        if key not in issue_id.keys():
            issue_id[key] = line
            #print(issue_id, end='\n')
    return(issue_id)

def writefile(key_issue):
        fname=['Issue_ID', 'Comments']
        with open('gitlab_dataop.csv', 'w') as opfile:
            writer = csv.DictWriter(opfile, fieldnames = fname)
            writer.writeheader()
            writer.writerow(key_issue)

gitcomments = pd.read_csv("gitlab_data1.csv")
issueid =gitcomments.groupby("Issue ID")
commentdicts = issueid.groups
key_issue = texmex(gitcomments, commentdicts)
for key, values in key_issue.items():
    print(str(key)+ " ********** "+str(values))
#writefile(key_issue)