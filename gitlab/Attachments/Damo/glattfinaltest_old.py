import requests
import os
import re
import pandas as pd

# Define the API URL for the GitLab issue
#project_id = "331"
#issue_id = "3667"
#issueatt = {}
#issue_api_url = "https://gitlab.atlas-se.net/api/v4/projects/331/issues/3667?private_token=glpat-v-96T6Fx6UxvvYLd58PJ&scope=all"
project_id = '331'
df = pd.read_csv(r"C:\Users\SESA731553\Documents\python\pyprograms3\gitlab\Attachments\testissueids.csv") #change file path containing Issue ID
issuelist =df['Issue ID']
issuelist = issuelist.tolist()
issueatt = {}

for each_issue in issuelist:
    issue_api_url = f"https://gitlab.atlas-se.net/api/v4/projects/{project_id}/issues/{each_issue}?private_token=glpat-v-96T6Fx6UxvvYLd58PJ&scope=all"
    response = requests.get(issue_api_url)
    response.raise_for_status()
    if response.status_code == 200:
        issue_data = response.json()
        if "description" in issue_data:
            upload_urls = re.findall(r'(/uploads/[^ ]+\.[a-zA-Z0-9]+)', issue_data["description"])
            if not upload_urls:
                print("{} issue dont have uploads in description".format(each_issue))
            else:
                print("{} uploads found in description".format(each_issue))
                for attachment in upload_urls:
                    if each_issue not in issueatt.keys():
                        issueatt[each_issue] = attachment
                    else:
                        issueatt[each_issue] += ", " + attachment
        #print(issueatt)

with open('git_issues_attachment', 'w', encoding = 'utf-8') as finalfile:
   for issueid, attachment in issueatt.items():
      finalfile.write("{}, {}\n".format(issueid, attachment))


