import requests, os, csv, json, gitlab, requests, pprint, sys, io
from PIL import Image
import pandas as pd
import argparse, re


# Define the API URL for the GitLab issue
project_id = "331"
issue_id = "3667"
dirpath = r"C:\Users\SESA731553\Documents\python\pyprograms3\gitlab\Attachments\Damo"
#issue_api_url = "https://gitlab.atlas-se.net/api/v4/projects/331/issues/3667?private_token=glpat-v-96T6Fx6UxvvYLd58PJ&scope=all"
issue_api_url = f"https://gitlab.atlas-se.net/api/v4/projects/{project_id}/issues/{issue_id}?private_token=glpat-v-96T6Fx6UxvvYLd58PJ&scope=all"
# Send a GET request to fetch data from the GitLab issue API
response = requests.get(issue_api_url)
response.raise_for_status()

# Check if the request was successful (status code 200)
if response.status_code == 200:
    issue_data = response.json()
    if "description" in issue_data:
        upload_urls = re.findall(r'(/uploads/[^ ]+\.[a-zA-Z0-9]+)', issue_data["description"])
        print("Attachments are : {}".format(upload_urls))
        gitlab_base_url = "https://gitlab.atlas-se.net/atals/atlas-product/"
        download_directory = os.path.join(dirpath , issue_id)

        if not os.path.exists(download_directory):
            os.makedirs(download_directory)

        # Download the attachments
        for upload_url in upload_urls:
            # Construct the full URL by adding the base GitLab URL
            full_attachment_url = gitlab_base_url + upload_url

            # Determine the filename from the URL
            attachment_filename = os.path.join(download_directory, os.path.basename(upload_url))

            # Send a GET request to download the attachment
            attachment_response = requests.get(full_attachment_url)
            attachment_response.raise_for_status()
            print("Attachment_response = {}".format(attachment_response))

            # Check if the request was successful (status code 200)
            if attachment_response.status_code == 200:
                if 'content-disposition' in attachment_response:
                    _, params = attachment_headers.headers['content-disposition'].split(';')
                    for parameter in params.split(';'):
                        key, value = parameter.strip().split('=')
                        if key == 'filename':
                            filenamex = value.strip("\\")
                            att_io = io.BytesIO(attachment_response)
                            if attachment_response[:4] == b'\x89PNG':
                                image = Image.open(att_io)
                                os.chdir(attachment_filename)
                                image.save('issue_id.png', 'PNG')
                            else:
                                fname = f'{filenamex}.xlsx'
                                df = pd.read_excel(att_io)
                                df.to_excel(fname)
                '''with open(attachment_filename, "wb") as file:
                    file.write(attachment_response.content)
                print(f"Attachment '{attachment_filename}' downloaded successfully.")
            else:
                print(f"Failed to download attachment. Status code: {attachment_response.status_code}")'''
    else:
        print("No description found in the issue data.")
else:
    print(f"Failed to fetch issue data. Status code: {response.status_code}")
