import requests, os, csv, json, gitlab, requests, pprint, sys, io
from PIL import Image
import pandas as pd
import argparse, re

dirpath = r"C:\Users\SESA731553\Documents\python\pyprograms3\gitlab\Attachments"
prefix = f"file://csvimport/atlas-admin/"

if len(sys.argv) < 2 :
	print("Run as: python {} pgid\n".format(sys.argv[0]))
	print("Optionally Specify custom Gitlab URL in OS envt var: GL_SERVER & export it")
	print("Default Gitlab URL is https://gitlab.atlas-se.net \n")

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.atlas-se.net')
GL_TOKEN = os.environ.get('GL_TOKEN', "glpat-v-96T6Fx6UxvvYLd58PJ")
parser = argparse.ArgumentParser()
parser.add_argument("pgid", help = "Enter the project group id, example 40 for Atlas.", type = int)
args = parser.parse_args()
print("Project Group id: {}".format(args.pgid))
print("Gitlab URL : {}".format(GITLAB_SERVER))
GROUP_ID = args.pgid

def file_name(issuecard):
	if 'content-disposition' in issuecard.headers:
		_, params = issuecard.headers['content-disposition'].split(';')
		for parameter in params.split(';'):
			key, value = parameter.strip().split('=')
			if key == 'filename':
				filename = value.strip("\\")
				return filename

'''if not GL_TOKEN:
	print("Please set environment variable for GL_TOKEN & export it")
	sys.exit(1)'''

#Instantiate Gitlab Client
gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GL_TOKEN, pagination="keyset", per_page=100)
main_group = gl.groups.get(GROUP_ID)
projectlist = main_group.projects.list(all=True)
for i in projectlist:
    projectid = i.id

issueslist = []

r = requests.get(f'{GITLAB_SERVER}/api/v4/projects/{projectid}/issues_statistics', headers={"PRIVATE-TOKEN": GL_TOKEN})
r.raise_for_status()
project_issues_stats = json.loads(r.text)

def project_issues_details(project_issue_stats):
    for key, value in project_issue_stats.items():
        if isinstance(value, dict):
            for key1,value1 in value.items():
                if isinstance(value1,dict):
                    for key2, value2 in value1.items():
                        if key2 == "all":
                            return(value2)

total_issues = project_issues_details(project_issues_stats)
import math; range_end = (math.ceil(total_issues/100)); range_end = range_end + 1
print("Page Counts {}".format(range_end))

def gather_issueiids(projectlist, range_end, projectid, issueslist):
    issuelink = i._links['issues']
    for pagecount in range(1,range_end):
        issuelink = f"{issuelink}?per_page=100&page={pagecount}"
        response = requests.get(issuelink, headers={"PRIVATE-TOKEN": GL_TOKEN})
        issue_data = response.json()
        for issueindx, issueitems in enumerate(issue_data):
            issueiid = (issue_data[issueindx]['iid'])
            issueslist.append(issueiid)
    return(issueslist)

allissuesiid = gather_issueiids(projectlist, range_end, projectid, issueslist)
#print(allissuesiid, end='\n\n\n')
#print(len(allissuesiid))
issuefiles = {}
for each_issue in allissuesiid:
	issueurl = f'https://gitlab.atlas-se.net/api/v4/projects/{projectid}/issues/{each_issue}?private_token={GL_TOKEN}&scope=all'
	issuecard = requests.get(issueurl)
	fname = file_name(issuecard)
	issuecard_json = issuecard.json()
	attachments = re.findall("/uploads/\w+.\w+.\w+", str(issuecard_json.get('description')))
	for attachment in attachments:
		if each_issue not in issuefiles.keys():
			issuefiles[each_issue] = attachment
		else:
			issuefiles[each_issue] += ", " + attachment
print(issuefiles)
