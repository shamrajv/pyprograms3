import pandas as pd
from functools import reduce

#All issue id column should be labeled as `Issue ID` in all input and output csv sheet
dfi = pd.read_csv("IssueIds.csv") 
dfo1 = pd.read_csv("attachment_links.csv")
dfo2 = pd.read_csv("all_issues_comments_grouped_d.csv")

dataframes = [dfi, dfo1, dfo2]
df_merged = reduce(lambda  left,right: pd.merge(left,right,on=['Issue ID'],how='outer'), dataframes)

df_merged.to_csv('FinalMerged.csv')

