The Access is removed for Chadi and Remy.
![image](/uploads/e578eb0f0ff7ddf3da18ef0746880884/image.png)



CC:@erambeaud
mentioned in commit 97d01ba21d8f99e030bce877fbed7ed89853334e
mentioned in merge request !635
created branch [`577-remove-chadi-and-remy-from-aws-account`](/atlas/atlas-admin/-/compare/master...577-remove-chadi-and-remy-from-aws-account) to address this issue
assigned to @sashangykumar
changed the description
