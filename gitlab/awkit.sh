#!/bin/bash

awk -F" " '{
    if (NR == 1) {
        # Skip the header row if it exists
        next;
    }

    # Extract the first field
    first_field = $1;

    # Check if the array already exists for this first_field
    if (!array[first_field]) 
      {
        # Create a new array element if it doesn't exist
        array[first_field] = $0;
    } else {
        # Append the current line to the existing array element
        array[first_field] = array[first_field] " " substr($0, index($0,$2));
    }

END {
    # Print the results
    for (key in array) {
        print "Array for " key ":\n" array[key];
    }}
}' gitlab_data1.csv
