import csv

data_dict = {}

with open('gitlab_data1.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader, None)

    for row in csv_reader:
        first_field = row[0]
        rest_of_line = ';'.join(row[1:])
        if first_field not in data_dict:
            data_dict[first_field] = rest_of_line
        else:
            data_dict[first_field] += '\t\t' + rest_of_line

# Print the results
for key, value in data_dict.items():
    print(f"{key}:{value}")
