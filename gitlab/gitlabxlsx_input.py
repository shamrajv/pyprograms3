import pandas as pd
import csv,re
import datetime as dt
import warnings
warnings.filterwarnings("ignore")


df = pd.read_excel("gitlab_input.xlsx")
tcolumn = df["Note Created At"]
row = tcolumn.shape
df["formatted_datetime"] = ""

(i,) = row
for datetime in range(i):
    dttime = re.split("T",tcolumn[datetime])
    time = dttime[1].split(".")   
    year = dttime[0].split('-')[0]
    month = dttime[0].split('-')[1]
    date = dttime[0].split('-')[2]
    mmddyyyy_t= ("{}/{}/{} {}".format(month,date,year,time[0]))
    #print(mmddyyyy_t)
    df["formatted_datetime"][datetime] = mmddyyyy_t

df['Comments'] = df[['formatted_datetime','Username','Note Body']].apply(lambda x: ';'.join(x), axis=1)
ndf = df[['Issue ID', 'Comments']]
ndf.to_excel('ndf.xlsx')

issueid =ndf.groupby("Issue ID")
commentdicts = issueid.groups
issuecomments = {}

for key, value in commentdicts.items():
    listofcomment_index = value.to_list()
    for each_index in listofcomment_index:
        if key not in issuecomments:
            issuecomments[key] = ndf.loc[each_index]["Comments"]
        else:
            issuecomments[key] += ", " + ndf.loc[each_index]["Comments"]

#with open('gitlab_commentsop.xlsx', 'w', encoding = 'utf-8') as finalfile:
 #   for issueid, comments in issuecomments.items():
  #      finalfile.write("{} {}\n".format(issueid,comments))

new_df = pd.DataFrame(data = issuecomments, index=[0])
new_df = (new_df.T)
new_df.to_excel('issuecomments.xlsx')