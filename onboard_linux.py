#Author : Vivek Shamraj / HSC Linux
#!/usr/bin/env python
import os, subprocess,csv,json,shlex,sys,signal

def append_server_details(datadict):
    with open('server_details.json', 'w') as wfile:
       json.dump(datadict, wfile, sort_keys=True, indent=4, separators=(',', ': '))

def create_server_details(filename):
   datadict = {}
   with open(filename, 'r') as data_file:
      data = csv.DictReader(data_file, delimiter=",")
      for row in data:
         item = datadict.get(row["org_name"], dict())
         item[row["servername"]] = row["svc_name"]
         datadict[row["org_name"]] = item
         append_server_details(datadict)

def exec_onboard_script():
   try:
      subprocess.check_call(shlex.split(" /opt/chefdk/embedded/bin/ruby /appl/chef_repo/chef_osc_other_orgs/bootstrap_other_orgs.rb"))
   except subprocess.CalledProcessError as err:
      print("Onboarding Error: " + str(err))

def main():
   create_server_details("servers.csv")
   exec_onboard_script()

try:
   main()
except KeyboardInterrupt:
   print "User Interrupt Recieved, Exiting"
   sys.exit(1)
