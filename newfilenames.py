filenames = ["program.c", "stdio.hpp", "sample.hpp", "a.out", "math.hpp", "hpp.out"]
# Generate newfilenames as a list containing the new filenames
# using as many lines of code as your chosen method requires.
newfilenames = []
for index, fnames in enumerate(filenames):
  if fnames.endswith('hpp'):
    filenames[index] = fnames.replace('hpp', 'h')

newfilenames.extend(filenames)
print(newfilenames)
 
# Should be ["program.c", "stdio.h", "sample.h", "a.out", "math.h", "hpp.out"]
