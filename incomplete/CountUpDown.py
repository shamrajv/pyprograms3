def counter(start, stop):
	x = start
	if x > stop:
		return_string = "Counting down: "
		while x >= stop:
			return_string += str(x)
			if x - 1 is not 0:
				return_string += ","
			return_string += str(x)
	else:
		return_string = "Counting up: "
		while x <= stop:
			return_string += str(x)
			if x + 1 is not stop:
				return_string += ","
			return_string += str(x)
	return return_string

print(counter(1, 10)) # Should be "Counting up: 1,2,3,4,5,6,7,8,9,10"
print(counter(2, 1)) # Should be "Counting down: 2,1"
print(counter(5, 5)) # Should be "Counting up: 5"
