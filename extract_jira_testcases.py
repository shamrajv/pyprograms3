import os, json, requests
import glob, re, sys, csv
import pandas as pd

def lol(customvalues):
    """Unpack the list of list into single list of all inclusive elements.
    Returns testcaselist"""
    testcaselist = []
    for item in customvalues:
        if isinstance(item, list):
            testcaselist.extend(item)
        else:
            testcaselist.append(item)
    return(testcaselist)

def createheaders(keypairs):
    """This function creates headerlist containing all customfield of given
    INSP record, as needed for its number of elements contained as list.
    Returns headerlist"""
    customkeys = list(keypairs.keys())
    customvalues = list(keypairs.values())
    multipliers = []
    for rowdata in customvalues:
        if isinstance(rowdata, list):
            multipliers.append(len(rowdata))
        else:
            multipliers.append('1')
    headerlist = []
    for header, count in zip(customkeys, multipliers):
        for varholder in range(int((count))):
            headerlist.append(header)
    return(headerlist)

def createrecfiles(headers, keypairs, key):
    """Creates individual records csv files"""
    print("Creating csv file for {}".format(key), end ="\n\n")
    headers.insert(0, "Issue Key")
    customvalues = list(keypairs.values())
    testcases = lol(customvalues)
    testcases.insert(0, key)
    fname = f"{key}.csv"
    with open(fname, 'w', encoding = 'utf-8') as keyfile:
        writer = csv.writer(keyfile)
        writer.writerow(headers)
        writer.writerow(testcases)

def mergecsvs(path, flat_superheaders, issuekeys):
    filelist = []
    for key in issuekeys:
        for files in os.listdir(path):
            if files.startswith(key) and files.endswith('csv'):
                filelist.append(os.path.realpath(files))
    mergeddf = pd.concat(map(pd.read_csv, filelist), ignore_index=True, keys = flat_superheaders)
    mergeddf.to_csv('merged_testcases.csv')

def main():
    path = r"target_dir_path"
    sourcejira = f"http://abc.xyz.jira:8080"
    keyfile = r"target_dir_path\Issuekeys.csv"
    df = pd.read_csv(keyfile)
    issuekeys = df["Key"]
    issuekeys = issuekeys.to_list()
    for key in issuekeys:
        #print(key)
        keypairs = f"{key}pairs"
        keypairs = dict()
        loks = list()
        sourceapi = f"/rest/api/2/issue/{key}"
        sourceurl = sourcejira + sourceapi
        print("Working on {} API response".format(key),end ="\n\n")
        try:
            response = requests.get(sourceurl, auth=('SESA575805', 'Thorrurwarangal@143'))
        except requests.exceptions.HTTPError as httperr:
            print("HTTP error occurred: " + str(httperr))
        except Exception as err:
            print("Exception occurred: " + str(err))
        if response.status_code == 200:
            response.encoding = 'utf-8'
            jsonfile = f"{key}record.json"
            with open(jsonfile, 'w', encoding="utf-8") as inputfile:
                json.dump(response.json(), inputfile, indent=4)

            with open(jsonfile, 'r') as jj:
                testdata = json.load(jj)
                customfield = testdata.get('fields') #value of "fields" containing dict of customfields and value as dict or list of dicts
                for cfield, val in customfield.items():
                    cfieldmatch = re.match("(customfield_\w+)", cfield) #take only customfields_[0-1]*
                    if cfieldmatch and customfield.get(cfield) is not None: # and they are not null
                        if isinstance(val, list):
                            if len(val) == 1:
                                #add to keypairs dictionary, the only "value" in list, if so
                                for index, curlylist in enumerate(val):
                                     #curlylist is the dict element in list of dictionary
                                     testcase = curlylist.get('value')
                                     keypairs[cfield] = testcase #Appending keypair customfield and value to per record dictionary
                            else:
                                lov = list() #stores list of values in dict of customfield_[0-9]
                                for index, curlylist in enumerate(val):
                                    testcase = curlylist.get('value')
                                    lov.append(testcase)
                                keypairs[cfield] = lov

                        if isinstance(val, dict) and "value" in val.keys():
                            #Add customefield and value to the dictionary
                            for keys, dictval in val.items():
                                testcase = val.get('value')
                                keypairs[cfield] = testcase
            print(keypairs, end="\n\n")
            headers = []
            headers.append(createheaders(keypairs))
            print(headers, end="\n\n")
            #superheaders = []
            #superheaders.append(headers)
            #print(superheaders)
            createrecfiles(headers, keypairs, key)
            #flat_superheaders = []
            #flat_superheaders.append(sorted(lol(headers)))
            #print(flat_superheaders, end ="\n\n")
            #mergecsvs(path, flat_superheaders, issuekeys)

try:
    flat_superheaders = main()
except KeyboardInterrupt:
    print("User Interrupted, exiting")
    sys.exit(1)
