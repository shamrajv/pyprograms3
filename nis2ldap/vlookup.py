# Purpose/ Banner : vlookup.py generates list of items that are unique to list(A) and not in list(B)
#                   as given in Masterfile(1st argument) and ProcessedFile (2nd Argument).
#                   Run in Sath server only.
# Version:        : 1.0
# Author:         : vivek.shamraj@dxc.com  itoindiagdc.dbtransunix@dxc.com
#!/usr/bin/env python2
import argparse
import os

parser = argparse.ArgumentParser(description='Provides Elements from 1st Argument Superset-File that are not there in 2nd Argumented File')
parser.add_argument('MasterFile', help='The First File from which we need unique items, not there in second file')
parser.add_argument('ProcessedFile', help='2nd Argumented File containing list of other items')

args = parser.parse_args()

A=[]
B=[]
with open(args.MasterFile, 'r') as file:
   for line in file.readlines():
      A.append(line.strip())

with open(args.ProcessedFile, 'r') as f:
   for line in f.readlines():
      B.append(line.strip())

C = set(A) - set(B)

for items  in list(C):
   print(items)
