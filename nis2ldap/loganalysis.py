#!/usr/bin/env python2.7

from __future__ import print_function
import re
import os



def gatherinfo(servername, text):
    result = re.findall(servername, text)
    return result


with open('servers','r') as f:
    for server in f.readlines():
        with open('precheck.log', 'r') as file:
            for line in file:
                chkresult = gatherinfo(server.strip(), line)
                if chkresult:
                    listline = []
                    listline.append(re.sub(server,'',line))
                    #modline = re.sub(server,'',line)
                    for lines in listline:
                        print(lines)

#New way

>>> newlist = []
>>> with open('precheck.log', 'r') as file:
...    for line in file.readlines():
...       if line.startswith(server):
...          newlist.append(re.sub(server,'',line.strip()))


print(*newlist, sep = ", ") # in python3
print(", ".join(newlist)) # in python2
print(server + ", ".join(newlist)) # improvement in python2
